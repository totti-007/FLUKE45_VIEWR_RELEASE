﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fluke45_viewer_txy.DataAnalysis
{
    class PointValue
    {
           private   double x;
           private  double y;
    
           //属性
           public double X
          {
              set { x = value; }
             get{return x;}
          }
           //属性
           public double Y
          {
              set { y = value; }
             get{return y;}
          }
    }
}
