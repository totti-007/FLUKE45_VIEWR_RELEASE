﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fluke45_viewer_txy.Fluke45;
using fluke45_viewer_txy.Target;
using fluke45_viewer_txy.About;
using fluke45_viewer_txy.DataAnalysis;
using System.Windows.Forms.DataVisualization.Charting;

namespace fluke45_viewer_txy
{
    public partial class FLUKE : Form
    {

        private fluke45 fluke45;
        private target target;
       // private double[][] 
       // private List<PointValue> PointValueArray = new List<PointValue>;
        private dataAnalysis dataAnalysis;

        public FLUKE()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            //调用系统默认的浏览器  
            System.Diagnostics.Process.Start("http://git.oschina.net/tanxiaoyao/FLUKE45_VIEWR_RELEASE");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            AboutUs aboutDlg = new AboutUs();
            aboutDlg.Show();
        }

        private void btn_clr_Click(object sender, EventArgs e)
        {
            text_data.Text = "";
            this.dataAnalysis.cleanArray();     //清空文本区同时清空数据记录器
        }

        private void btn_connect_Click(object sender, EventArgs e)
        {
            //生成端口
            if(fluke45 == null)
                fluke45 = new fluke45(dropdown_fluke.Text);  //指定万用表物理端口
            if(target == null)
            {
                if(dropdown_target.Text!= dropdown_fluke.Text)
                    target = new target(dropdown_target.Text);  //指定目标设备物理端口
                else
                {
                    MessageBox.Show("请选择一个和万用表不同的端口！");
                    return;
                }
            }
            //尝试连接万用表以及设备端口
            if (!fluke45.isConnected)
            {
                fluke45.OpenChanelFluke();//打开通道
                if(!fluke45.isConnected)
                {
                    MessageBox.Show("万用表连接失败，请检查设置！");
                    return;
                }
                else
                {
                    if(target.isConnected)
                    {
                        btn_connect.Text = "断开";
                        label_state.Text = "连接中";
                        label_state.ForeColor = Color.Green;
                        return;
                    }
                }
            }
            if(!target.isConnected)
            {
                target.OpenChanelTarget();//打开通道
                if(!target.isConnected)
                {
                    MessageBox.Show("目标设备连接失败，请检查设置！");
                    return;
                }
                else
                {
                    if(fluke45.isConnected)
                    {
                        btn_connect.Text = "断开";
                        label_state.Text = "连接中";
                        label_state.ForeColor = Color.Green;
                        return;
                    }
                }
            }
            //所有设备都连接上了，这时本按钮是断开连接功能
            if(fluke45.isConnected && target.isConnected)
            {
                fluke45.CloseChannelFluke();
                target.CloseChanelTarget();
                label_state.Text = "未连接";
                label_state.ForeColor = Color.Red;
                btn_connect.Text = "连接";
            }
        }

        private void FLUKE_Load(object sender, EventArgs e)
        {
            //窗口创建的时候加载各种基本变量
            dataAnalysis = new dataAnalysis(); //创建数据分析器

        }

        private void ben_sample_Click(object sender, EventArgs e)
        {
            try
            {
                double valueFluke = fluke45.getFlukeValue();
                double valueTarget = target.getTargetValue();
                text_data.AppendText(valueTarget.ToString() + "\t\t" + valueFluke.ToString() + "\n");
                dataAnalysis.addPoint(valueTarget, valueFluke);       //将新测得的数据添加到集合中
            }
            catch
            {
            }
        }

        private void dropdown_fluke_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropdown_target.Text != dropdown_fluke.Text)
                fluke45 = new fluke45(dropdown_fluke.Text); //端口改变，重新生成一个万用表串口
            else
            {
                MessageBox.Show("请选择一个和目标设备不同的端口！");
                return;
            }
        }

        private void dropdown_target_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (dropdown_target.Text != dropdown_fluke.Text)
                target = new target(dropdown_target.Text);  //指定目标设备物理端口
            else
            {
                MessageBox.Show("请选择一个和万用表不同的端口！");
                return;
            }
        }

        private void ben_analysis_Click(object sender, EventArgs e)
        {
            if (this.dataAnalysis.analysExistingData())
            {
                MessageBox.Show("y = " + dataAnalysis.k_para.ToString() + " * x + " + dataAnalysis.b_para.ToString(), "线性拟合", MessageBoxButtons.OK,
                MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
            }
        }
    }
}
