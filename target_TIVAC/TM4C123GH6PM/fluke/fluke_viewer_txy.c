/*
 * fluke_viewer_txy.c
 *
 * 本驱动程序将配合FLUKE VIEWER TXY上位机使用，上位机向设备发送一个c字符
 * 本程序就会将用户绑定的变量值发送到上位机以供分析。
 * 本软件自由使用，遵守MIT开源协议，任何人可以自由修改分发本软件
 *
 *  Created on: 2015年8月9日
 *      Author: tanxiaoyao
 */

#include "fluke_viewer_txy.h"

extern volatile double charge_cur;
extern volatile double output_volt;
extern volatile double charge_volt;

volatile double *monitorData;		//监视的数据，bind的时候将目标数的指针传过来就可以实现实时跟踪了

/********************初始化设备管脚、晶振等***************************/
void initFVT() {
	//配置UART0为9600,8-N-1
	ROM_UARTConfigSetExpClk(UART0_BASE, ROM_SysCtlClockGet(), 9600,
			(UART_CONFIG_WLEN_8 |
			UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));
	//使能串口中断
	ROM_IntEnable(INT_UART0);
	ROM_UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);
	ROM_IntMasterEnable();
	uint32_t ulStatus;
	//获取中断状态
	ulStatus = ROM_UARTIntStatus(UART0_BASE, true);
	//清除中断标志
	ROM_UARTIntClear(UART0_BASE, ulStatus);
}

/*****************绑定要监视的数据***************************/
void bindDataFVT(volatile double *sendData) {
	monitorData = sendData;
}

/*****************打印字符函数**************************/
void printCharFVT(unsigned char sendChar) {
	ROM_UARTCharPut(UART0_BASE, sendChar);
}

/****************打印字符串函数************************/
void printStringFVT(char *sendString) {
//	ROM_IntDisable(INT_UART0);
	ROM_UARTIntDisable(UART0_BASE, UART_INT_RX | UART_INT_RT);		//暂时关闭中断
	while ((*sendString) != '\0') {
		printCharFVT(*sendString);
		sendString++;
	}
//	ROM_IntEnable(INT_UART0);
	ROM_UARTIntEnable(UART0_BASE, UART_INT_RX | UART_INT_RT);		//重新开始接收
}

/***************接收字符串*************************/
//char *tempStr;
char buffer[512] = "";
void getStringFVT() {
	if (ROM_UARTCharGet(UART0_BASE) == 'c') {//判断上位机发送的请求验证符（为了加快速度及验证简便，仅用一个c字符验证）
		sprintf(buffer, "%lf=>\n", charge_volt);	//double转string
		printStringFVT(buffer);
	}
}

//串口接收中断
void flukeViewerHandler() {
	uint32_t ulStatus;
	//获取中断状态
	ulStatus = ROM_UARTIntStatus(UART0_BASE, true);
	//清除中断标志
	ROM_UARTIntClear(UART0_BASE, ulStatus);
	//直到串口FIFO中没有数据时才退出循环
	while (ROM_UARTCharsAvail(UART0_BASE)) {
		getStringFVT();			//接收字符串
	}
}
